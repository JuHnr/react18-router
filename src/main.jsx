import React from "react";
import ReactDOM from "react-dom/client";

//install and import react router
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";

//créé les routes (url) correspondantes à chaque "page"
const router = createBrowserRouter([
  {
    path: "/", //route
    element: <Home/>, //associe le composant à afficher en accédant à cette route (la "page")
  },
  {
    path: "/products",
    element: <ProductList />,
  },
  {
    path: "/cart",
    element: <Cart />,
  },
  
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* use react router*/}
    <RouterProvider router={router} />
    
  </React.StrictMode>
);
