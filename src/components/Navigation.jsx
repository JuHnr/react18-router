import "./Navigation.css";
import { Link } from "react-router-dom";

function Navigation() {
  return (
    <ul className="Navigation">
      {/*add links to the different pages / créé la navigation*/}
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/products">Products</Link>
      </li>
      <li>
        <Link to="/cart">Cart</Link>
      </li>
    </ul>
  );
}

export default Navigation;
